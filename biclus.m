tic
%% biclustering algorithm

% clc 
% clear


% % data 1
% load filteredyeastdata 
% data=yeastvalues;

% % data 2
% x = zeros(150,10);
% % x_row = 1:15,16-18,19-80,81-105,106-125,126-150
% % x_col = 1:4,5-6,7-14,15-20
% x(1:15,7:10) = 1;
% x(1:15,11:14) = 0.6;
% x(16:18,5:6) = 0.6;
% x(19:80,1:4) = 0.4;
% x(81:105,7:14) = 0.8;
% x(106:125,15:20) = 1;
% x(126:150,1:4) = 0.5;
% 
% 
% A = x+0.01*randn(size(x));


% % data 3
% x = [0 0 1 0 0 1 0 1;
%      0 0 0 0 0 0 0 0;
%      0 1 0 1 0 0 1 0;
%      0 0 0 0 0 0 0 0;
%      0 0 1 0 0 1 0 1;
%      0 1 0 1 0 0 1 0];
% 
% A = x+0.01*randn(size(x));

% % data 4
% % Only param ishaan 
% data = gene(100,100);

% % data 5
% data =data_nii;


%% data standardization

% the data is strandadized by taking mean of 0 and standard deviation of 1 along any
% required side ie row or column.


A = zscore(data_nii,[],2);


[numRow,numCol] = size(A); 
% row_name = {'A','B','C','D','E','F'};
% i was trying to assign alphabets to rows
row_name = num2cell(1:numRow);
col_name = num2cell(1:numCol);


%% hierarchical clustering

% choose no of k clusters
% clusters can be chosen only to indicate bigger clusters
k_row = numRow  ; k_col = numCol;


method = 'average';
metric = 'euclidean';

% build hierarchical tree clustering row and column separately
 % for row 
tree_row = linkage(A,method,metric);

D_Row= pdist(A);
leafOrder_row = optimalleaforder(tree_row,D_Row);

 % for column  
tree_col = linkage(A',method,metric);

D_Col= pdist(A');
leafOrder_col = optimalleaforder(tree_col,D_Col);

% Group the row according to the desired number
figure; 
subplot(2,1,1);
[~,row_clust_idx,perm_row] = dendrogram(tree_row,k_row,'Reorder',leafOrder_row);
title('dendrogram of row');

% Group the col according to the desired number
subplot(2,1,2);
[~,col_clust_idx,perm_col] = dendrogram(tree_col,k_col,'Reorder',leafOrder_col);
title('dendrogram of column');


%%  Get the permutation vector
perm_col = perm_col(:)';
colmagic = 1:numCol(:);
col_clust_idx = col_clust_idx(:);
x_index = []';
x_label = []';

for l = perm_col
   
    members_in_label = colmagic(col_clust_idx==l);
    x_index = [x_index, members_in_label];
    x_label = [x_label, l+zeros(1,length(members_in_label))];
end


 % for row 
perm_row = perm_row(:)';
rowmagic = 1:numRow(:);
row_clust_idx = row_clust_idx(:);
y_index = [];
y_label = [];
for l = perm_row
    
    members_in_label = rowmagic(row_clust_idx==l);
    y_index = [y_index, members_in_label];
    y_label = [y_label, l+zeros(1,length(members_in_label))];
end


%% Plot the result

A_x = A(:,x_index);
A_xy = A_x(y_index,:);
figure;  imagesc(A);
%xsubplot(2,2,1); imagesc(A);
% subplot(2,2,2); imagesc(A_xy); title('column cluster');
% set(gca,'XTick',1:numCol); set(gca,'XTickLabel',a(y_index));
% daspect([1 0.5 1]);
% subplot(2,2,3); imagesc(row_clust_idx(y_index(:))); title('row cluster');
% set(gca,'YTick',1:numRow); set(gca,'YTickLabel',row_clust_idx(y_index));
% daspect([0.003 1 1])
%subplot(2,2,2); imagesc(A_xy);
figure; imagesc(A_xy);
set(gca,'YTick',1:numRow); set(gca,'YTickLabel',row_name(y_index)); 
set(gca,'XTick',1:numCol); set(gca,'XTickLabel',col_name(x_index));
colorbar
toc
%%   bicluster for mse score

% % for row clusters 
% 
% 
% 
% 
% row_cls_idx= zeros(numRow,1,k_row);
% 
% for i = perm_row;
%     
%   pr_row_cls_idx= ismember(row_clust_idx,i);  
%   
%   row_cls_idx(:,:,i) = pr_row_cls_idx;
%     
% end
% 
% row_cls_idx= reshape(row_cls_idx,numRow,k_row);
% 
% row_cls_idx= logical(row_cls_idx);   % for logical indexing 
% 
% % for column clusters
% 
% col_cls_idx= zeros(numCol,1,k_col);
% 
% for i = perm_col;
%     
%   pr_col_cls_idx= ismember(col_clust_idx,i);  
%   
%   col_cls_idx(:,:,i) = pr_col_cls_idx;
%     
% end
% 
% col_cls_idx= reshape(col_cls_idx,numCol,k_col);
% 
% col_cls_idx= logical(col_cls_idx);    % for logical indexing 

% %% for biclusters
%     
% bcls= [];
% 
% for r= perm_row  
%    cls_row= A_xy(row_cls_idx(:,r),:); 
%    
%    for s= perm_col
%        
%     bcls(:,:,s)= cls_row(:,col_cls_idx(:,s)');
%            
%    end
%   
% end

%%

% % Remove zero rows
% data( all(~data,2), : ) = [];
% % Remove zero columns
% data( :, all(~data,1) ) = [];

% shuffledArray = BA(randperm(size(BA,1)),:);
% figure; stairs(cumsum(c)/sum(c))