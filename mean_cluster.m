tic
%% Algorithm Mean clustering

% clc
% clear
%% data input and standardization

% % Test Data
% load filteredyeastdata.mat
% in_data=zscore(yeastvalues,[],2);

% %  Data Input
% load result_new
% in_data=zscore(data_nii,[],2);
% in_data=part_1;
% data_nii=A;
% [numRow,numCol] = size(b);
% in_data=data_nii;

%% hierarchical clustering for row
% choose no of k clusters % clusters can be chosen only to indicate bigger clusters
b=data_nii;
[numRow,numCol] = size(b);
k_row = numRow  ; k_col = numCol;


method = 'complete'; metric = 'cosine';

% build hierarchical tree clustering row and column separately %  for row
tree_row = linkage(b,method,metric);

D_Row= pdist(b, metric); 
leafOrder_row =optimalleaforder(tree_row,D_Row);

[~,row_clust_idx,id_row]=dendrogram(tree_row,k_row,'Reorder',leafOrder_row);

%% mean clustering for row


% mean_row=mean(data_nii,1);
% 
% dist_row=pdist2(data_nii,mean_row,'euclidean');
% 
% [~,id_row] = sort(dist_row,'descend');

% id_row=kmeans(dist_row,83);


%% Mean clustering for column 
% note :- heirachical clustering consider larger distance whereas mean
% clustering neglects by mean zero distance
% data_nii_2=log(data_nii);
% data_nii_2=real(data_nii_2);

mean_col=mean(data_nii,2);

dist_col=pdist2(data_nii',mean_col','cosine');

[~,id_col] = sort(dist_col,'descend');

%% Plot the result

A_x = data_nii(id_row,:);
A_xy = A_x(:,id_col);
% figure; imagesc(data_nii);colormap(hsv)
% figure; imagesc(A_xy);colormap(hsv)
figure;
subplot(1,2,1); imagesc(data_nii);
subplot(1,2,2); imagesc(A_xy);
colorbar
% colormap(jet)
% colormap(redgreencmap)
colormap(colorcube)
% colormap(hsv)
toc



