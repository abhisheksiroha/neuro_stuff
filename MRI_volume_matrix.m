% This script is for extracting volumetric data from .nii images to
% give a resulting matrix
% The rows of matrix are subjects whereas columns are voxels

% created by 
% Abhishek Siroha
% M.Tech
% IIT Guwahati
% 164106003

%% 
% clear;
clc;

% create a empty matrix 
BA=[];

%% prepare file to load in loop from current follder
filefolder=fullfile(pwd);
files=dir(fullfile('*.nii'));
files2={files(:).name};
files1=char(files2);
files1=cellstr(files1);
files1=natsort(files1);
files1=char(files1);
%% loop every images to convert it in matrix form 

for i=1:numel(files)
% read volume in three dimensional matrix 
% A=spm_read_vols(spm_vol('3dataset_group_loading_coeff_.nii'))
AA=spm_read_vols(spm_vol(files1(i,:)));
% convert 3D matrix into row arrays

AB= AA(:)';
% concatenate the row arrays into single matrix

BA=[BA;AB];

end
['Done']


