% clear all
clc
% close all

%% 
addpath(genpath('/hd/archive/matlab/spm12'));
o=1;
% 1 is patient and 2 is control hence comparing
% load covariates
%  Define filepath and filename of the sMRI image with header to read into the Matlab workspace
datadir = pwd;
fname = files1(o,:);
Data2Read = fullfile(datadir,fname);
HeaderInfo = spm_vol(Data2Read);
% HeaderInfo=rmfield(HeaderInfo,'pinfo');  % remove pinfo variable from this struct

%% ALL MRIdata size 91x109x91xnsubs
%Remove Nans Intersect with mean mask from FSL

% MRIdata= spm_read_vols(spm_vol(char(TXT(:,1))))

MRIdata= spm_read_vols(spm_vol(files1)); % MRI data
[a1,a2,a3,numsub] = size(MRIdata);
data11 = reshape(MRIdata,a1*a2*a3,numsub);
ind11 = find(~isnan(sum(data11,2)));% finds row which are not nans
data12 = data11(ind11,:);
% finds non zero voxel rows
ind12=find(sum(data12,2));
ind1 = ind11(ind12);% non zero voxels and non nans for all subjects
% ind1 = ind11(data12);
figure;montage(reshape(mean(data12'),[a1 a2 1 a3]))

anat=spm_read_vols(spm_vol('/hd/alz2/mean.nii'));%use whatever mask you want here or one you define from the mean
anatomy=anat;
[indm1,indm2,indm3] = intersect(find(anatomy),ind1);
rdata=data11(indm1,:);

%%% end of preprocessing and flattening %%%

%% Voxel wise regression of Age and Gender for subjects
% X=ori_age;
X=[age,sex,ones(85,1)];

alpha=0.05;
[vv mm]=size(rdata);
rdata_mod=zeros(vv,mm);P_values=zeros(1,mm);
tic
for ii=1:size(rdata,1);
    dfree=numsub- size(X,2);
    tval = tinv((1-alpha/2),dfree);
    if sum(rdata(ii,:)~=0)
        
        % P(ii)=anova1(DTI_data(ii,:),X);
        [b,bint,r,rint,stats]=regress(rdata(ii,:)',X,alpha);% sub by voxel
        % p value
        t= b(end)/((bint(end,2)-bint(end,1))/2/tval );% t value for SNP( 1 or less is not significant)
        P_values(ii)=2*min(tcdf (t, dfree), 1-tcdf(t, dfree)) ;% pa value
        rdata_mod(ii,:)=rdata(ii,:)'-X(:,1:size(X,2)-1)*b(1:size(X,2)-1);
        %Panova(ii)=anova1(rdata(ii),X(:,1:size(X,2)-1));
        
    else
        P_values(ii)=0;
    end
end
% RECONSTRUCTING AND WRITING wc1 IMAGES after regressing for AGE/GENDER
prefix = 'r';
for ii=1:size(rdata_mod,2)

% reconstruct the full size volume for display 
comp1=zeros(a1,a2,a3);
comp1(indm1)=rdata_mod(:,ii);
comp=reshape(comp1,[a1 a2 a3]);


[pathstr,name,ext]=fileparts(files2{ii});
aa=fullfile([prefix name ext]);
  

% Step 1.  Take the header information from a previous file with similar dimensions and voxel sizes and change the filename in the header.
HeaderInfo.fname = aa; %aa;  % This is where you fill in the new filename
HeaderInfo.dt(1) = 4;
%HeaderInfo.private.dat.fname = HeaderInfo.fname;  % This just replaces the old filename in another location within the header.
cd('/hd/alz2/regressed');
spm_write_vol(HeaderInfo,comp);
    
end

